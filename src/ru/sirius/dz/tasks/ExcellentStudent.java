package ru.sirius.dz.tasks;

public class ExcellentStudent extends Students{

    public ExcellentStudent(String name, String group) {
        super(name, group);
    }

    public void saySmartThought() {
        System.out.println("Жизнь — как вождение велосипеда. Чтобы сохранить равновесие, ты должен двигаться");
    }
}
