package ru.sirius.dz.tasks;

public class IteratorClass implements java.util.Iterator<Students>{
    private int current = 0;
    private int limit = 10;

    private Students[] students;

    public IteratorClass() {
        this.students = new Students[limit+1];

        for (int i = 0; i < limit+1; i++) {
            students[i] = new Students("" + i, "" + i);
        }
    }


    public boolean hasNext() {
        return this.current <= this.limit;
    }

    public Students next() {
        this.current++;
        return students[this.current - 1];
    }
}
