package ru.sirius.dz.tasks;

public class SchoolWorker implements SchoolInterface{
    private final String name;
    public int salary;

    public SchoolWorker(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return this.name;
    }

    public int getSalary() {
        return this.salary;
    }
}
