package ru.sirius.dz.tasks;

public class Students implements SchoolInterface{
    private final String name;
    public String group;

    public static int kol = 0;

    public Students(String name, String group) {
        this.name = name;
        this.group = group;
        kol++;
    }

    public String getName() {
        return this.name;
    }

    public String getGroup(){
        return this.group;
    }

    public int getKol() {
        return kol;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
