package ru.sirius.dz.tasks;

import java.util.Random;

public class Main {
    static String[] names = {"Bob", "Bill", "Michael", "Jacob", "Ethan", "William", "David", "John"};
    static String[] groups = {"1.1", "6.1", "7.1", "7.2"};

    public static void main(String[] args) {
        Students[] students = new Students[5];

        for (int i = 0; i < students.length; i++) {
            String name = names[new Random().nextInt(names.length)];
            String group = groups[new Random().nextInt(groups.length)];
            students[i] = new Students(name, group);
        }
        System.out.println(students[0].getKol());

//--------------------------------------------------------------------------------------------------------------------//
        SchoolInterface[] persons = new SchoolInterface[5];

        for (int i = 0; i < 3; i++) {
            String name = names[new Random().nextInt(names.length)];
            String group = groups[new Random().nextInt(groups.length)];
            persons[i] = new Students(name, group);
        }
        for (int i = 3; i < persons.length; i++) {
            String name = names[new Random().nextInt(names.length)];
            persons[i] = new SchoolWorker(name, (int) (Math.random() * 20000));
        }

        for (int i = 0; i < persons.length; i++) {
            persons[i].sayHurray();
        }
    }
}
