package ru.sirius.lecture.iterator;

public class ArrayWithoutArray implements java.util.Iterator<Integer>{
    private int current = 0;
    private int limit;

    ArrayWithoutArray(int limit) {
        this.limit = limit;
    }

    public boolean hasNext() {
        return this.current <= this.limit;
    }

    public Integer next() {
        int value = this.current;
        this.current++;
        return value;
    }
 }
