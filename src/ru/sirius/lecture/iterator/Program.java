package ru.sirius.lecture.iterator;

import ru.sirius.dz.tasks.IteratorClass;
import ru.sirius.dz.tasks.Students;

public class Program {
    public static void main(String[] args) {
        IteratorClass array = new IteratorClass();
        while(array.hasNext()) {
            Students s = array.next();
            System.out.println(s.getName() + " " + s.getGroup());
        }

    }
}
