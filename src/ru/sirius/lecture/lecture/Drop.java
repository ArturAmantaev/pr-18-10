package ru.sirius.lecture.lecture;

import java.io.*;

public class Drop {
    private String message;
    private boolean empty = true;

    private String readFile() {
        BufferedReader bufferedReader = null;
        String strFile = "";
        try {
            bufferedReader = new BufferedReader(new FileReader("resources/myText.txt"));
            strFile = bufferedReader.readLine();

            System.out.println("Считал:" + strFile);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return strFile;
    }

    private void writeFile(String str) {
        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter("resources/myText.txt"));
            bufferedWriter.write(str);
            System.out.println("Записал:" + str);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public synchronized String take() {
        while (empty) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }

        String str = readFile();
        writeFile("");

        empty = true;
        notifyAll();
        return message;
    }

    public synchronized void put(String message) {
        while (!empty) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }

        String str = readFile();
        if (str != "") {
            writeFile("erhguehfo");
        }

        empty = false;
        this.message = message;
        notifyAll();
    }
}